import socket
import os
s = socket.socket()

PORT = 1235
s.bind(('', PORT))
s.listen(8)


while True:
    conn, addr = s.accept()
    conn.send('    Connected, waiting for a filename\n'.encode())
    RecvData = conn.recv(1024)
    basename = RecvData.decode()
    conn.send(f'    Receiving {basename}...\n'.encode())
    (basename, extension) = os.path.splitext(basename)
    fname = f'{basename}{extension}'
    copy = 1
    # while there are some copys, we simply increment counter  and chech if this name is free
    while(os.path.isfile(fname) or os.path.isdir(fname)):
        fname = f'{basename}_copy_{copy}{extension}'
        copy += 1
    RecvData = conn.recv(1024)
    # we have a free name, so we can start writing to a file
    file = open(fname, "wb")
    while RecvData:
        file.write(RecvData)
        RecvData = conn.recv(1024)
    conn.send(f'    {fname} is received, closing connection\n'.encode())
    conn.close()
    file.close()
