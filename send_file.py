import socket
import sys
import os
import tqdm
import threading


class ThreadUpload(threading.Thread):
    def __init__(self, fname, sock):
        threading.Thread.__init__(self)
        self.fname = fname
        self.sock = sock

    def run(self):
        s = self.sock
        # send the filename to server
        s.send(f'{os.path.basename(fname)}'.encode())
        filesize = os.path.getsize(self.fname)
        # define the progress bar
        progress = tqdm.tqdm(
            range(filesize),
            f"Sending {os.path.basename(fname)}",
            unit="B",
            unit_scale=True,
            unit_divisor=1024,
        )

        file = open(self.fname, "rb")
        SendData = file.read(1024)

        while SendData:
            #send a chunk of data, read one more and update progress bar
            s.send(SendData) 
            SendData = file.read(1024)
            progress.update(len(SendData))
        progress.close()
        file.close()


class ThreadListen(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock

    def run(self):
        while self.sock.fileno() > 0: # While socket is open
            print(f'{bcolors.OKBLUE}### Server:{bcolors.ENDC}\n{sock.recv(1024).decode()}{bcolors.OKBLUE}###{bcolors.ENDC}')


class bcolors: # some constants for stdout colors
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


try: # check if any argument is missing
    (fname, addr, port) = sys.argv[1:]
except Exception:
    print(f'{bcolors.FAIL}Arguments are not well-formatted. Please specify filename, address and port number{bcolors.ENDC}')
else:
    sock = socket.socket()
    sock.connect((addr, int(port)))
    sender = ThreadUpload(fname, sock)
    listener = ThreadListen(sock)
    # start child processes
    sender.start()
    listener.start()
    # wait till the file is sent
    sender.join()
    # close the socket, listener will see it is closed and terminate
    sock.shutdown(socket.SHUT_WR)
    sock.close()
